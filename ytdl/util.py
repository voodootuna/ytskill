import youtube_dl

def search_track(search_arg, audio_format=0):
    track = ''
    ydl_opts = {
            'format': 'bestaudio/best',
             'noplaylist': True,
        }
    with youtube_dl.YoutubeDL(ydl_opts) as yt:
        info = yt.extract_info("ytsearch:{}".format(search_arg), download=False)
        track_info = info['entries'][0]
        track_audio_url = track_info['formats'][audio_format]['url']
        track_info = {
                'artist':track_info.get('artist') or track_info.get('channel'),
                'title':track_info.get('title'),
                'url':track_audio_url,
                'image':track_info.get('thumbnail') or None
        }

    return track_info



def track_info(search_arg, audio_format=0):
    ydl_opts = {
            'format': 'bestaudio/best',
             'noplaylist': True,
    }
    with youtube_dl.YoutubeDL(ydl_opts) as yt:
        info = yt.extract_info("ytsearch:{}".format(search_arg), download=False)
        track_info = info['entries'][0]

    return track_info['thumbnail']