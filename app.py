import logging
import gettext
import os

from flask import Flask
from ask_sdk_core.skill_builder import SkillBuilder
from flask_ask_sdk.skill_adapter import SkillAdapter
from ask_sdk_core.dispatch_components import (
    AbstractRequestHandler, AbstractExceptionHandler,
    AbstractRequestInterceptor, AbstractResponseInterceptor)
from ask_sdk_core.utils import is_request_type, is_intent_name
#from ask_sdk_core.handler_input import HandlerInput
from ask_sdk_model import Response
from alexa import data, util
from ytdl.util import search_track

app = Flask(__name__)

sb = SkillBuilder()

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# ######################### INTENT HANDLERS #########################
# Handlers for the built-in intents and generic
# request handlers like launch, session end, skill events etc.


class LaunchRequestHandler(AbstractRequestHandler):
    
    """Handler for generic launch intent"""
    
    def can_handle(self,handler_input):
        # type: (HandlerInput) -> bool
        return is_request_type("LaunchRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("In LaunchRequestHandler")
        handler_input.response_builder.speak(data.WELCOME_MSG).set_should_end_session(False)
        return handler_input.response_builder.response
        

class PlayAudioHandler(AbstractRequestHandler):
    
    """Handler for PlayAudio intent."""
    
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_intent_name("PlayAudioIntent")(handler_input)

    def handle(self, handler_input):
        query = handler_input.request_envelope.request.intent.slots[
                "query"].value.lower()
        track = search_track(query, audio_format=data.AUDIO_FORMAT)
        # type: (HandlerInput) -> Response
        logger.info("In PlayAudioHandler")
        return util.play(url=track['url'],
                         offset=0,
                         text=data.NOW_PLAYING_MSG.format(track['title']),
                         card_data=util.card_data(title=track['title'], image=track['image']),
                         response_builder=handler_input.response_builder)


class CancelOrStopIntentHandler(AbstractRequestHandler):
    
    """Handler for generic cancel, stop or pause intents."""
    
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return (is_intent_name("AMAZON.CancelIntent")(handler_input) or
                is_intent_name("AMAZON.StopIntent")(handler_input) or
                is_intent_name("AMAZON.PauseIntent")(handler_input))

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("In CancelOrStopIntentHandler")
 
        return util.stop(data.STOP_MSG, handler_input.response_builder)



class HelpIntentHandler(AbstractRequestHandler):
    
    """Handler for providing help information."""
    
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_intent_name("AMAZON.HelpIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("In HelpIntentHandler")
        handler_input.response_builder.speak(data.HELP_MSG).set_should_end_session(False)
        return handler_input.response_builder.response


class PlaybackFinishedHandler(AbstractRequestHandler):
    
    """AudioPlayer.PlaybackFinished Directive received.
    Confirming that the requested audio file completed playing.
    Do not send any specific response.
    """
    
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_request_type("AudioPlayer.PlaybackFinished")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("In PlaybackFinishedHandler")
        logger.info("Playback finished")
        return handler_input.response_builder.response


# ################## TO DO #############################

#Implement other Intents

# ################## EXCEPTION HANDLERS #############################
class CatchAllExceptionHandler(AbstractExceptionHandler):
    
    """Catch all exception handler, log exception and
    respond with custom message.
    """
    
    def can_handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> bool
        return True

    def handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> Response
        logger.info("In CatchAllExceptionHandler")
        logger.error(exception, exc_info=True)
        
        handler_input.response_builder.speak(data.UNHANDLED_MSG).ask(
            data.HELP_MSG)

        return handler_input.response_builder.response

# ###################################################################

sb.add_request_handler(LaunchRequestHandler())
sb.add_request_handler(PlayAudioHandler())
sb.add_request_handler(CancelOrStopIntentHandler())
sb.add_request_handler(PlaybackFinishedHandler())
sb.add_request_handler(HelpIntentHandler())
# Exception handlers
sb.add_exception_handler(CatchAllExceptionHandler())

skill_response = SkillAdapter(
    skill=sb.create(),
    skill_id='amzn1.ask.skill.0bc37a08-c240-42e7-b60d-e81ef23cf3eb',
    app=app, verifiers=[])



skill_response.register(app=app, route="/") # Route API calls to / towards skill response
app.run(debug=True)

